# Firebase

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Qu'est-ce-que Firebase](#quest-ce-que-firebase)
- [Comment créer un projet sur Firebase](#comment-cr%C3%A9er-un-projet-sur-firebase)
- [Installation des outils](#installation-des-outils)
  - [Sur la machine](#sur-la-machine)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Qu'est-ce-que Firebase

Firebase est un service d'hebergement gratuit proposé par Google permettant d'héberger ses données, sites web etc en m ode AAS ( Architecture As A Service).

## Comment créer un projet sur Firebase

1. Aller sur le site `https://firebase.google.com`.
1. Cliquer sur Sign in.

    ![img](tools/firebase/Image/FirebaseScreen1.png)

    > Vous allez devoir créer un compte Google si vous n'en avez pas.

1. Add new project
    ![img](tools/firebase/Image/FirebaseScreen2.png)
    > Une fois crée, le projet sera hebergé dans la partie Hosting.

## Installation des outils

### Sur la machine

 1. aller sur le terminal
 1. Ecrire la commande `npm i -g firebase-tools`

    > Si l'installation de se fait pas sur MAC, il faut vérifier les droits sur le dossier en utilisant la commande `sudo chown -R $USER:$(id -gn $USER) $HOME/.config`.

 1. Verifier la version en ecrivant `firebase -V`.
 1. Saisir la commande `firebase init` pour initialiser firebase.
 1. Ecrire `firebase login` si vous n'etes pas connectés.
    > Suivre les instructions du terminal.
 1. Ecrire la commande `firebase deploy` pour deployer l'application.
 1. Cliquer sur le lien correspondant à votre projet afin de l'afficher sur le Web.
