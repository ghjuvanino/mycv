<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
- [Post](#post)
  - [Exemple d'utilisation](#exemple-dutilisation)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Post
Il s'agit d'une méthode qui permet d'ajouter des données sur le serveur.
## Exemple d'utilisation

``` javascript
import express from 'express';
import bodyParser from "body-parser";

const app = express();

app.use(bodyParser({}));
let users = [
    {
    id:1,
    firstName: 'toto',
    age: 20,
},
{
    id:2,
    firstName: 'titi',
    age: 21,
},
{
    id:3,
    firstName: 'tete',
    age: 12,
},
    ];
app.post('/api/users',
    (req, res) => {
    const newUser= req.body;
    const id = users.length+1;
    users.push({...newUser,id});
        return res.send(users);
    });
```

Sur le debug `Postman`, on sélectionne la méthode `Post`. On va donc rajouter une ressource supplémentaire à notre array comme suit:

![img](tools/nodejs/images/post1.png)

Le résultat final :

![img](tools/nodejs/images/post2.png)
