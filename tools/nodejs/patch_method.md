<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
- [Patch](#patch)
  - [Exemple d'utilisation](#exemple-dutilisation)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Patch

Il s'agit d'une méthode qui permet de modifier partiellement les ressources, comme modifier un attribut d'un objet.

## Exemple d'utilisation

``` javascript
import express from 'express';
import bodyParser from "body-parser";

const app = express();

app.use(bodyParser({}));

let users = [
    {
    id:1,
    firstName: 'toto',
    age: 20,
},
{
    id:2,
    firstName: 'titi',
    age: 21,
},
{
    id:3,
    firstName: 'tete',
    age: 12,
},
    ];

app.patch('/api/users/:id',
    (req, res) => {
const newUser= req.body;
const id = parseInt(req.params.id);
const index= users
     .findIndex((user)=>user.id === id);
users[index]= {...users[index],...newUser};
return res.send(users);
    });
```
Sur le debug`Postman` on sélectionne la méthode Patch.
On va donc modifier partiellement une des ressources on modifiant un attribut de l'un des objets comme suit:.

![img](tools/nodejs/images/patch1.png)

Le résultat final:

![img](tools/nodejs/images/patch2.png)
