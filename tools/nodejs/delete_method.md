<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Delete](#delete)
  - [Exemple d'utilisation](#exemple-dutilisation)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Delete
Il s'agit d'une méthode qui permet d'effacer des ressources.

## Exemple d'utilisation

``` javascript
let users = [
    {
    id:1,
    firstName: 'toto',
    age: 20,
},
{
    id:2,
    firstName: 'titi',
    age: 21,
},
{
    id:3,
    firstName: 'tete',
    page: 12,
},
    ];

app.delete('/api/users/:id',
    (req, res) => {
    const id= parseInt(req.params.id);
    console.log(id);
    users= users.filter((user)=>user.id !== id);
    return res.send(users);
    });
```
Sur le debuf `Postman`, on selectionne délete. Au niveau de l'api,on rajoute l'index 2 après users.
` http://localhost:3015/api/users/2` 

Ceci veut dire qu'on va supprimer l'objet dont l'ID est 2.
Le résultat final sera le suivant :

![img](tools/nodejs/images/delete2.png)
