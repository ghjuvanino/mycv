<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Architecture client/serveur](#architecture-clientserveur)
  - [Schéma architecture client/serveur](#sch%C3%A9ma-architecture-clientserveur)
  - [Le port](#le-port)
    - [Changer de port](#changer-de-port)
      - [Webpack](#webpack)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


# Architecture client/serveur

Une architecture client serveur est un mode de communication à travers un réseau de plusieurs programme. autrement dit, il s'agit d'une communication entre des clients qui emettent des requêtes via leur adresse et leur port et des serveurs qui reçoivent les demandes et qui y répondent à l'adresse du client et de son port.

## Schéma architecture client/serveur

```mermaid
sequenceDiagram
         client1 ->> server: GET /api HTTP/1.1 
         server -->> client1:200 OK 
```

## Le port

Le port permet d'accéder aux programmes serveurs/clients. un port est identifié par un entier et il en existe 65535.

- Les ports de 0 à1023, contrôlés et assignés par l'IANA, sont dénommés Well Known Ports.
- Les ports 1024 à 49151 sont appelés ports enregistrés (Registered Ports)
- Les ports 49152 à 65535 sont les ports dynamiques ou privés
### Changer de port
#### Webpack
le module Webpack permet de changer le port du serveur `Node.js` .

Il arrive parfois, qu'on ait besoin d'exécuter le serveur `Node.js` deux fois sur la même machine. le port ayant un numéro unique, il n'est possible d'exécuter deux applications  en simultanée que si on change le numéro de port. en `Node.js` cela est faisable en allant sur le module`Webpack.config.js`. Pour cela il suffit de changer le numéro de port en mettant 3015 au lieu de 3000 initialement, à titre d'exemple.
