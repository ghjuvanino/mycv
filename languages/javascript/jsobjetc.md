
**Table of Contents**
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Object.keys](#objectkeys)
- [Object.values](#objectvalues)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

En JavaScript, il arrive parfois, de devoir transformer des objets en tableau afin de faciliter le traitement des données.

## Object.keys

`Object.keys()` est une méthode qui permet de renvoyer en forme de tableau les clés de l'objet.

> Exemple
> ```JavaScript
> const sharkFamily = {
>   familyName = shark;
>   firstName = baby;
>   age = 1;
> }

> Utilisons `Object.keys()`pour extraire les clés de cet objet:

> ```JavaScript
> const familyKeys = Object.keys(sharkFamily);
> console.log(familyKeys);
>```
> Résultat
> ['familyName','firstName','age']

## Object.values

`Object.values()` est une méthode qui permet de renvoyer en forme de tableau les valeurs de l'objet.

> Exemple

> ```JavaScript
> const sharkFamily = {
>   familyName = shark;
>   firstName = baby;
>   age = 1baby;
> }

> Utilisons `Object.values()`pour extraire les clés de cet
objet:

> ```JavaScript
> const familyValue = Object.values(sharkFamily);
> console.log(familyValue);
>```

> Résultat
> ['shark','baby','1]
