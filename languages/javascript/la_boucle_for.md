<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Loop](#loop)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Loop
La boucle `for` sert à répéter une action plusieurs fois en faisant des boucles.
> Exemple

> ```JavaScript
> for(i=0,i<20,i=i+1);
> console.log(i);
> ```

> Résultat
> On aura 20 fois l'élément `i`.

La boucle for se compose de trois parties:

- `i=o` : c'est l'initiateur. il s'exécute une seule fois. Il permet de créer la variable et l'initialiser avant le lancement de la boucle.
- `i<20` : c'est la condition de continuation (boolean).
- `i=i+1` : cette partie s'exécute tant que la condition n'est pas remplie.
