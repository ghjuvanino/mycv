<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  

- [Organiser une feuille de CSS](#organiser-une-feuille-de-css)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Organiser une feuille de CSS

Pour organiser le CSS et structurer son projet,va regrouper plusieurs feuilles CSS comme suit:
1. Aller dans le dossier *assets* et céer un dossier *css*.
1. On déplace ensuite les fichiers style.css dans ce dossier. L’IDE remplace l’ancien css chemin par le nouveau dans les fichiers html.
1. on crée un nouveau fichier css qu’on appellera par exemple `main` et qui regroupera tout le css utilisé pour la classe main.
1. il faudrait ensuite  aller dans le fichier  ’style’ et créer un import comme suit:

    ``` css
    @import url('main.css’)
    ```

    le fichier qui regroupera les imports ira chercher les propriétés css dans les fichier  d’origine.
