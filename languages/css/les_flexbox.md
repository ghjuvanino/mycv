<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Les flexbox](#les-flexbox)
  - [Propriétés des flexbox](#propri%C3%A9t%C3%A9s-des-flexbox)
    - [Quelques propriétés parents](#quelques-propri%C3%A9t%C3%A9s-parents)
    - [Quelques propriétés child](#quelques-propri%C3%A9t%C3%A9s-child)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Les flexbox

Les flexbox font partie des élèments du CSS native.
Le principe des flexbox est de donner à un element contenant appelé *container* la possibilité de de changer les dimensions des élements contenus appelés *items*.
le but de cela étant de s’adapter à toutes les tailles d’écran, tous les terminaux et de mieux occuper l’espace disponible.

## Propriétés des flexbox

certaines propriétés s’appliquent (container) au parent et d'autres au (child.).

### Quelques propriétés parents

- Display: définit si un container est flex, cette propriété s’appliquera à tous les items du container.
- Flex-direction: établit l’axe principal.
  - Row: de gauche à droite.
  - Row reverse: de droite à gauche.
  - Column: de haut en bas.
  - Column reverse: de bas en haut.
- Flex-wrap: permet aux elements de tenir dans la meme page notamment lorqu’ils sont trop larges.
- justify-content: définit l’alignement le long de l’axe principal ( row ou column).
- align-items: définit l’alignement le long de l’axe cross.

### Quelques propriétés child

- Flex-grow: définit l’espace que va prendre un item à l’interieur du container. un Item ayant la valeur 2 prendra deux fous plus de place qu’un item ayant la veleur 1.
- Flex-shrink: permet de rétrecir la taille des item. La valeur par défaut du shrink est de 1. pour rétrécir l’image lui accorde la valeur 0.
