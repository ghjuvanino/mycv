<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Un peu d'histoire](#un-peu-dhistoire)
  - [Les interfaces](#les-interfaces)
    - [Syntaxe d'une interface](#syntaxe-dune-interface)
  - [Les Namespaces](#les-namespaces)
  - [Les modules](#les-modules)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Un peu d'histoire

Typescript est un langage  qui a été conçu par Anders Hejlsberg, également concepteur du langage C#. Le but premier de TypeScript est de rendre plus facile et plus fiable l'écriture de code en JavaScript pour des applications de grande ampleur.

## Les interfaces

Une interface permet de donner un nom à un objet et de typer ses propriétés.

### Syntaxe d'une interface

Une interface se présente comme suit:

``` javascript
interface MyInterface {
  prop1: type;
  prop2: type;
  prop3: type;
```

> Ne pas oublier  d'utiliser la notation PascalCase pour le nom de l’interface.
> Ne pas oublier de mettre un point-virgule à la fin de chaque ligne dans l’interface.

## Les Namespaces

Les namespaces permettent d'organiser les variables dans un groupe donné afin d'éviter les problèmes d'écrasement des variables.

## Les modules

Les modules sont des Namespace dans un fichier isolé.
Exemple

``` javascript
export interface RoomModel {
    id: number;
    size: number;
    roomName: string;
}
```
