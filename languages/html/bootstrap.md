# Bootstrap

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents*-  

- [Qu'est ce que Bootstrap](#quest-ce-que-bootstrap)
- [Installer Bootstrap](#installer-bootstrap)
- [Intégrer bootstrap à du HTML](#int%C3%A9grer-bootstrap-%C3%A0-du-html)
  - [Activer Bootstrap](#activer-bootstrap)
  - [Quelques exemples de l'application de bootstrap](#quelques-exemples-de-lapplication-de-bootstrap)
- [Les containers](#les-containers)
- [Row et Col](#row-et-col)
- [Créer des liens](#cr%C3%A9er-des-liens)
- [Modifier bootstrap](#modifier-bootstrap)
  - [Modifier la classe bootstrap](#modifier-la-classe-bootstrap)
  - [Surcharger la classe bootstrap](#surcharger-la-classe-bootstrap)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Qu'est ce que Bootstrap

- Framework permettant de structurer et d'encadrer notre facon de travailler.

## Installer Bootstrap

- Aller dans Terminal
- Saisir : npm init => entrer =>entrer =>entrer => Name (MONCV)
  - Un fichier package.json va se creer
- Saisir : npm install bootstrap => Entrer
  - Un fichier package-lock.json va se creer

## Intégrer bootstrap à du HTML

On va créer un fichier *html- qu'on  appelera index.html

### Activer Bootstrap

Saisir dans index.html: `<link rel="stylesheet" href="../node_modules/bootstrap/dist/css/bootstrap.css">`

> il faut remonter d'un niveau pour aller sur Bootstrap d'ou les deux points ../
> On appuie sur control+esp et une liste s'affiche. on remonte vers le fichier recherché.

### Quelques exemples de l'application de bootstrap

- On crée  dans le body une DIV comportant une class principale "container'
  - On crée une grille composée de lignes (ROW) et de colonnes ( columns).

  ``` html
  div class="container">
          <div class="row">
              <div class="col numbers">1</div>
              <div class="col numbers">2</div>
              <div class="col numbers">3</div>
          </div>
  ```

- On rajoute un style qu'on appelera **numbers*- afin de centrer les colonnes en creant un file Styles.css.

``` css
  .numbers{
         text-align: center;
     }
```

On rattache le fichier style.css en rajoutant une ligne link comme suit:

`<link rel="stylesheet" href="../asset/style.css">`

On remonte d'un niveau vers `assets` puis `stylesheet`.

>PS: On peut voir que les colonnes numbers sont centrees en inspectant la page qu'on ouvre via index.html sur le navigateur.

## Les containers

Il existe deux types de containers:

- Un container standard qui reste fixe.
- Un container fluid: Qui s'adapte å la page et prend toute sa largeur.

## Row et Col

Col et Row fonctionnent toujours ensemble: Pas de Row sans Col et vis versa.
Row est l'element principal contenant des Col.

Un element est toujours wrappé dans un Row qui a sa Col.
Bootstrap contient 12 colonnes.
Pour décaler une colonne vers la gauche on utilise la fonction: Offset. Ceci est possible lorsque la totalite des colinnes est <12.

Le contenu sert à faire apparaitre les couleurs des colonnes.  On peut utiliser HTML entities **&nbsp*- pour mettre du ciontenu sans saisir du texte ou definir une height et widh.

Contrarement aux ROW, les COLS ont une longueur ce qui fait qu'on ne les considere pas comme vides.

## Créer des liens

Il faut utilidser la balise `<a> </>` et remoplir la valeur `href="..."` par le chemin approprié.
Ex:

```html
<a href="../mondossier/monfichier.xyz">TEXT to show</a>
```

> Note:
>
> 1. `.` signifie dossier courant.
> 1. `..` signifie dossier parent.

## Modifier bootstrap

Pour modifier bootstrap il est possible de soit:

- créer des thémes bootstrap : cela des du domaine de l'expertise en CSS.

  - Modifier la classe bootstrap dans le css.

- Surchacharger la classe bootdstrap en rajoutant une autre classe la représentant.

### Modifier la classe bootstrap

pour cela il suffit d’appliquer du CSS sur la classe bootstrap.

  Exemple, changer la couleur de la classe `btn primary` :

- On crée une classe `btn-primary`.
- On applique un background colour différent de la couleur primary ‘green’ à titre d’exemple.
- la couleur green s’appliquera sur le btn à condition que le link  stylesheet bootstrap soit aprés le link stylesheet CSS.

### Surcharger la classe bootstrap

Pour cela il suffit de rajouter une classe supplémentaire qui completera la classe d’origine de de lui appliquer du CSS
Exemple, changer la couleur de la classe' btn primary:

- On rajouter une classe ‘my-button’ qui completra la classe btn-primary.
  - Dans le fichier 8css8, on applique le background green à la classe .my-button.
- La couleur green s’appliquera à condition que le link stylesheet *Css- vienne aprés le link stylesheet *Bootstrap*.

 A noter:

 > Il n’est pas recommadé de modofier directentbn le fichier bootstrap se trouvant das le vendors.
 par exemple, pour modifier la couleur d’une row, il est interdit de faire cela dnas le fichier 'assets/vendors/css/bootstrap.css’.
  >Cela modifiera cette classe pour toute les personne travaillant sur le meme projet.
